# Copyright 2017 Rasmus Thomsen
# Distributed under the terms of the GNU General Public License v2

require github [ user=phw tag=${PV} ] vala [ vala_dep=true ] cmake [ api=2 ]
require gtk-icon-cache gsettings

SUMMARY="Simple screen recorder with an easy to use interface"

PLATFORMS="~amd64 ~x86"
SLOT="0"
LICENCES="GPL-3"

MYOPTIONS="
    gnome-shell [[ description = [ Enable GNOME Shell recorder ] ]]
    ( providers: ffmpeg libav ) [[ number-selected = exactly-one ]]
"

# Unpackaged gifski, optional runtime dependency
DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19]
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.42]
        dev-libs/keybinder:3.0
        media-plugins/gst-plugins-good:1.0[gstreamer_plugins:vpx] [[
            note = [ Required for near lossless compression of GIFs ]
        ]]
        x11-libs/cairo
        x11-libs/gtk+:3[>=3.22]
        providers:ffmpeg? ( media/ffmpeg[h264][vpx][X] )
        providers:libav? ( media/libav[h264][vpx][X] )
    suggestion:
        media-libs/gifski [[
            description = [ Required for rendering high quality GIFs ]
        ]]
        media-plugins/gst-plugins-ugly:1.0[gstreamer_plugins:h264] [[
            description = [ Required for H264 encoding of video ]
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=( -DGSETTINGS_COMPILE=OFF -DBUILD_TESTS=OFF )

CMAKE_SRC_CONFIGURE_OPTIONS=( '!gnome-shell DISABLE_GNOME_SHELL' )

# Requires access to the X/Wayland session
RESTRICT="test"

pkg_postrm() {
    gtk-icon-cache_pkg_postrm
    gsettings_pkg_postrm
}

pkg_postinst() {
    gtk-icon-cache_pkg_postinst
    gsettings_pkg_postinst
}

