# Copyright 2008 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2009 Daniel Mierswa <impulze@impulze.org>
# Copyright 2015 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require mozilla-nightly [ co_project=mail ]

SCM_REPOSITORY="https://hg.mozilla.org/comm-central"

SCM_firefox_REPOSITORY="https://hg.mozilla.org/mozilla-central"
SCM_firefox_UNPACK_TO="${WORKBASE}/${PNV}/mozilla"
# In case we also want to build firefox-nightly
SCM_firefox_CHECKOUT_TO="firefox-nightly"

SCM_SECONDARY_REPOSITORIES="firefox"

require scm-hg

SUMMARY="Mozilla's standalone mail and news client"

LICENCES="MPL-2.0"

PLATFORMS="~amd64 ~x86"

MOZILLA_SRC_CONFIGURE_PARAMS+=(
    --enable-webspeech
    --enable-calendar           # (lightning calendar)
)

src_compile() {
    # Right now Thunderbird tries to download blessings-1.6 ( even if installed ) when
    # it sets its virtualenv up, which fails when we have our net sandbox enabled.
    # Thunderbird actually provides blessings 1.3 though, so this is most likely a bug.
    esandbox disable_net

    mozilla-nightly_src_compile

    esandbox enable_net
}

src_install() {
    mozilla-nightly_src_install

    insinto /usr/share/applications
    doins "${FILES}"/${PN}.desktop

    edo pushd "${IMAGE}"

    edo mv usr/$(exhost --target)/lib/thunderbird* \
        usr/$(exhost --target)/lib/thunderbird-nightly

    # Don't collide with mail-client/thunderbird
    edo rm usr/$(exhost --target)/bin/thunderbird
    dosym /usr/$(exhost --target)/lib/thunderbird-nightly/thunderbird \
        /usr/$(exhost --target)/bin/thunderbird-nightly

    edo popd
}

