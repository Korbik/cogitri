# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=adapta-project tag=${PV} ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="An adaptive Gtk+ theme based on Material Design Guidelines"

LICENCES="
    GPL-2
    CCPL-Attribution-ShareAlike-4.0 [[ note = [ icons ] ]]
"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# Some dependencies are only mentioned in the readme and not in the configure script
DEPENDENCIES="
    build:
        dev-lang/sassc[>=3.3]
        media-gfx/inkscape[>=0.91]
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.53] [[ note = [ 2.53 require for GNOME 3.26 support ] ]]
        dev-libs/libxml2:2.0
        x11-libs/gdk-pixbuf:2.0[>=2.32.2]
        x11-libs/gtk+:3[>=3.20]
        x11-themes/gtk-engines-murrine[>=0.98.1]
    recommendation:
        fonts/google-roboto-fonts [[ description = [ primary font used by adapta ] ]]
        fonts/noto [[ description = [ secondary font used by adapta ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-gtk_next
    --enable-plank
    --enable-telegram
    --enable-tweetdeck

    # Disable support for stuff that's not in the tree anymore
    --disable-chrome_legacy

    # We use EXJOBS
    --disable-parallel
)

